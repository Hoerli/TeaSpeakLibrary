#pragma once

namespace tr {
    enum Messages {
        kick_invalid_badges,
        kick_invalid_packet,
        kick_invalid_command,

        crash_instance,
        shutdown_instance,
        shutdown_server,
    };
}